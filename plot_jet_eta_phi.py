### Skeleton for plot_jet_eta_phi.py
# created by HP Bretz
# Fri Jul 20 13:15:11 2012
# Modified by JS
### PyAnalyse skeleton script

import pxl
from pxl import modules
import ROOT
import numpy

class Example(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        #self._exampleVariable = startValue

    def initialize(self, module):
        ''' Initialize module options '''
        module.addOption("jetEventViewName", "Name of the EventView containing the jets", "Reconstructed")
        module.addOption("clusterEventViewName", "Name of the EventView containing the clustered particles", "Generated")
        module.addOption("jetName", "Name of the jets", "jet")
        module.addOption("displayClusterParticles", "Whether to also plot the clustered particles", True)
        module.addOption("minJetPt", "Minimum pT threshold for jets to be drawn", 2.)
        module.addOption("maxNjets", "Maximum number of jets to be drawn", -1)
        module.addOption("segmentationEta", "Number of eta bins", 100)
        module.addOption("segmentationPhi", "Number of phi bins", 100)
        module.addOption("SaveToFigure", "Save figures, leave empty to display instead of save", "etaphi.png")
        self.__module = module
        self.__counter = 0

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job: Draw jet eta-phi plot'
        self.__jetEventViewName = self.__module.getOption("jetEventViewName")
        self.__clusterEventViewName = self.__module.getOption("clusterEventViewName")
        self.__jetName = self.__module.getOption("jetName")
        self.__displayClusterParticles = self.__module.getOption("displayClusterParticles")
        self.__minJetPt = self.__module.getOption("minJetPt")
        self.__maxNjets = self.__module.getOption("maxNjets")
        self.__segmentationEta = self.__module.getOption("segmentationEta")
        self.__segmentationPhi = self.__module.getOption("segmentationPhi")
        self.__saveToFigure = self.__module.getOption("SaveToFigure")

    def beginRun(self):
        '''Executed before each run'''
        pass

    def analyse(self, object):
        '''Executed on every object'''
        event = core.toEvent(object)
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetPalette(1)
        
        # Identify eventviews
        eventViews = event.getEventViews()
        
        jetEventView = 0
        clusterEventView = 0
        
        for ev in eventViews:
            if ev.getName() == self.__jetEventViewName:
                jetEventView = ev
            elif ev.getName() == self.__clusterEventViewName:
                clusterEventView = ev
        
        #Identify jets
        jets = []
        maxJetPt = 0.

        if jetEventView != 0:
            particles = jetEventView.getParticles()
            for jet in particles:
                if jet.getName() == "jet":
                    if jet.getPt() > self.__minJetPt:
                        if jet.getPt() > maxJetPt:
                            maxJetPt = jet.getPt()
                        jets.append(jet)

        # Sort jets by pt
        jets = sorted(jets, key = lambda jet: -jet.getPt())
        
        #Identify related clustered particles
        clusterPs = []
        
        if self.__displayClusterParticles:
            for jet in jets:
                if clusterEventView != 0:
                    rels = jet.getSoftRelations().getSoftRelativesOfType(clusterEventView.getObjectOwner())
                    clusterPs.append(rels)
        
        
        # Make the plots
        c1 = ROOT.TCanvas()
        hists = []

        for i, jet in enumerate(jets):
            if self.__maxNjets > 0 and i >= self.__maxNjets:
                break
            
            hist = ROOT.TH2F(("h_%d"%self.__counter)+"_"+str(i), "Reconstructed Jets", self.__segmentationEta, -5., 5., self.__segmentationPhi, -numpy.pi, numpy.pi)
            jethist = ROOT.TH2F(("h_%d"%self.__counter)+"_"+str(i)+"jet", "Clustered Jets", self.__segmentationEta, -5., 5., self.__segmentationPhi, -numpy.pi, numpy.pi)

            hist.GetXaxis().SetTitle("#eta")
            hist.GetYaxis().SetTitle("#phi")
            hist.GetZaxis().SetTitle("p_{T} / GeV")
            hist.GetZaxis().SetRangeUser(0., maxJetPt * 1.05)
            hist.SetFillColor(i+1)
            hist.SetLineColor(i+1)
            hist.SetMarkerColor(i+1)
            
            jethist.GetXaxis().SetTitle("#eta")
            jethist.GetYaxis().SetTitle("#phi")
            jethist.GetZaxis().SetTitle("p_{T} / GeV")
            jethist.GetZaxis().SetRangeUser(0., maxJetPt * 1.05)
            jethist.SetFillColor(1)
            jethist.SetLineColor(1)
            jethist.SetMarkerColor(1)

            jethist.Fill(jet.getEta(), jet.getPhi(), jet.getPt())
            for clusterP in clusterPs[i]:
                hist.Fill(clusterP.getEta(), clusterP.getPhi(), clusterP.getPt())
            if i == 0:
                hist.DrawCopy("LEGO1 0")
                jethist.DrawCopy("LEGO 0 SAME")
            else:
                hist.DrawCopy("LEGO1 0 SAME")
                jethist.DrawCopy("LEGO 0 SAME")
        
        c1.Update()

        if self.__saveToFigure == "":
            c1.WaitPrimitive()
        else:
            fname, delimiter, extension = self.__saveToFigure.rpartition('.')
            c1.Print("%s%d%s%s"%(fname, self.__counter, delimiter, extension))
        self.__counter += 1

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job:  Draw jet eta-phi plot'
