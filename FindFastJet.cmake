# - Try to find FastJet 
# Once done, this will define
#
#  FastJet_FOUND - system has FastJet 
#  FastJet_INCLUDE_DIRS - the FastJet include directories
#  FastJet_LIBRARIES - link these to use FastJet 
# Note that this only configures the FastJet-core system to add a FastJet
# plugin, use the FastJet_ADD_PLUGIN(name), where name is e.g. FastJet-astro


# Use pkg-config to get hints about paths
FIND_PACKAGE(PkgConfig)
if (PKG_CONFIG_FOUND)
	pkg_check_modules(FastJet_PKGCONF FastJet)
	SET(FastJet_PLUGIN_INSTALL_PATH $ENV{HOME}/${FastJet_PLUGIN_INSTALL_PATH})
endif (PKG_CONFIG_FOUND)

# Include dir
find_path(FastJet_INCLUDE_DIRS
  NAMES fastjet/ClusterSequence.hh
  HINTS ${FastJet_PKGCONF_INCLUDE_DIRS}  /usr/local/include/fastjet/
)

# library 
find_library(FastJet_LIBRARIES
  NAMES fastjet
  HINTS ${FastJet_PKGCONF_LIBRARY_DIRS} /usr/local/lib/
)




