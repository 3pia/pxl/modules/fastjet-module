#include "fastjet/ClusterSequence.hh"
#include "fastjet/SISConeSphericalPlugin.hh"
//#include "siscone/spherical/siscone.h"
#include <iostream>
#include "pxl/modules/Module.hh"
#include "pxl/modules/ModuleFactory.hh"
#include "pxl/core.hh"
#include "pxl/hep.hh"
#include "pxl/core/Event.hh"
#include "pxl/hep/EventView.hh"

using namespace pxl;
using namespace fastjet;
using namespace std;

class FastJetModule: public pxl::Module {

	private: 
		long _Count;

		double _kt_algorithm_R;
		double _cambridge_algorithm_R;
		double _antikt_algorithm_R;
		double _genkt_algorithm_R;
		double _genkt_algorithm_p;
		double _ee_genkt_algorithm_R;
		double _ee_genkt_algorithm_p;
		
		string _jet_algorithm;
		string _view_name_input;
		string _view_name_output;

		vector<Particle*> _particles;
		map <string, JetAlgorithm> algorithms;

	public:
	
	// initialize Module
	FastJetModule() : 
		Module() {
		// we need to tell the sink that this module should be
		// called when data was sent to it
		_Count = 0;
	}

	// destructor
	~FastJetModule() {
	}
	
	void initialize() throw (std::runtime_error) {
		addSink("input", "DefaultInput");
		addSource("output", "DefaultOutput");
		
		addOption("view_name_input", "EventView in the input file to use for the jet reconstruction", "Generated");
		addOption("view_name_output", "EventView in the output file to use for the jet reconstruction", "Reconstructed");

		addOption("jet_algorithm", 
				"Available algorithms are those for which parameters are defined below as well as 'ee_kt_algorithm'.", 
				"kt_algorithm");

		algorithms.insert( pair<string, JetAlgorithm>("kt_algorithm", kt_algorithm));
		algorithms.insert( pair<string, JetAlgorithm>("cambridge_algorithm", cambridge_algorithm));
		algorithms.insert( pair<string, JetAlgorithm>("antikt_algorithm", antikt_algorithm));
		algorithms.insert( pair<string, JetAlgorithm>("genkt_algorithm", genkt_algorithm));
		algorithms.insert( pair<string, JetAlgorithm>("ee_kt_algorithm", ee_kt_algorithm));
		algorithms.insert( pair<string, JetAlgorithm>("ee_genkt_algorithm", ee_genkt_algorithm));

		addOption("kt_algorithm_R", "jet radius R for the kt algorithm", 1.);

		addOption("cambridge_algorithm_R", "jet radius R for the cambridge/aachen algorithm", 1.);

		addOption("antikt_algorithm_R", "jet radius R for the antikt algorithm", 1.);

		addOption("genkt_algorithm_R", "jet radius R for the generalised kt algorithm", 1.);
		addOption("genkt_algorithm_p", "continuous parameter p for the generalised kt algorithm", -1.);

		addOption("ee_genkt_algorithm_R", "jet radius R for the generalised kt algorithm for ee", 1.);
		addOption("ee_genkt_algorithm_p", "continuous parameter p for the generalised kt algorithm for ee", -1.);


	}

	void beginJob() throw (std::runtime_error) {
		getOption("kt_algorithm_R", _kt_algorithm_R);
		getOption("cambridge_algorithm_R", _cambridge_algorithm_R);
		getOption("antikt_algorithm_R", _antikt_algorithm_R);
		getOption("genkt_algorithm_R", _genkt_algorithm_R);
		getOption("genkt_algorithm_p", _genkt_algorithm_p);
		getOption("ee_genkt_algorithm_R", _ee_genkt_algorithm_R);
		getOption("ee_genkt_algorithm_p", _ee_genkt_algorithm_p);

		getOption("jet_algorithm", _jet_algorithm);

		getOption("view_name_input", _view_name_input);
		getOption("view_name_output", _view_name_output);
	}

	bool analyse(Sink* sink) throw (std::runtime_error) {
		Event *event = dynamic_cast<Event *> (getSink("input")->get());
		if (event) {
			vector<PseudoJet> pjets ;
			getPseudojets(event, pjets);
			applyJetDefinition(event, algorithms[_jet_algorithm], pjets);
			
		}
		else {
			cerr << "Something other than an Event passed by";
		}
		getSource("output")->setTargets(getSink("input")->get());
		_particles.clear();
		return getSource("output")->processTargets();

	}

	// get vector of Pseudojets from the particles in the event
	// save selected particles in vector
	void getPseudojets(Event* event, vector<PseudoJet> &pjets) {
		vector<EventView*> eventViews;
		event->getObjectsOfType<EventView>(eventViews);

		// get EventView "Generated"
		for (unsigned i = 0; i < eventViews.size(); i++) {
			if (eventViews[i]->getName() == _view_name_input) {
				vector<Particle*> particles;
				eventViews[i]->getObjectsOfType<Particle>(particles);

				// get all particles with status > 0 (particles that didn't decay
				int counter = 0;
				for (unsigned j = 0; j < particles.size(); j++) {
					int status = particles[j]->getUserRecord("status");
					if (status > 0) {
						_particles.push_back(particles[j]);
						PseudoJet pj( particles[j]->getPx(), particles[j]->getPy(), 
								particles[j]->getPz(), particles[j]->getE() );
						pj.set_user_index(counter); //associate pj with particle
						pjets.push_back(pj);
						counter++;
					}
				}
			}
			else {
				cout << "EventView " << eventViews[i]->getName() << " not used" << endl;
			}
		}
	}

	void applyJetDefinition(Event* event, JetAlgorithm jet_algorithm, vector<PseudoJet> &pjets) {
		switch (jet_algorithm) {
			case kt_algorithm : {
					JetDefinition jet_def(kt_algorithm, _kt_algorithm_R);
					ClusterSequence cs(pjets, jet_def);
					addJetsToEvent(event, cs, "kt_algorithm");
					break;
			}
			case cambridge_algorithm : {
					JetDefinition jet_def(cambridge_algorithm, _cambridge_algorithm_R);
					ClusterSequence cs(pjets, jet_def);
					addJetsToEvent(event, cs, "cambridge_algorithm");
					break;
			}
			case antikt_algorithm : {
					JetDefinition jet_def(antikt_algorithm, _antikt_algorithm_R);
					ClusterSequence cs(pjets, jet_def);
					addJetsToEvent(event, cs, "antikt_algorithm");
					break;
		    }
			case genkt_algorithm : {
					JetDefinition jet_def(genkt_algorithm, _genkt_algorithm_R, _genkt_algorithm_p);
					ClusterSequence cs(pjets, jet_def);
					addJetsToEvent(event, cs, "genkt_algorithm");
					break;
			}
			case ee_genkt_algorithm : {
					JetDefinition jet_def(ee_genkt_algorithm, _ee_genkt_algorithm_R, _ee_genkt_algorithm_p);
					ClusterSequence cs(pjets, jet_def);
					addJetsToEvent(event, cs, "ee_genkt_algorithm");
					break;
			}
			case ee_kt_algorithm : {
					JetDefinition jet_def(ee_kt_algorithm);
					ClusterSequence cs(pjets, jet_def);
					addJetsToEvent(event, cs, "ee_kt_algorithm");
					break;
			}
			default : 
				break;
		}
	}

	void addJetsToEvent(Event* event, ClusterSequence &cs, string algorithm) {
		vector<PseudoJet> jets = cs.inclusive_jets();
		
		EventView* eventView = new EventView();
		eventView->setName(_view_name_output);
        
		for (unsigned i = 0; i < jets.size(); i++) {
			Particle* particle = new Particle();
			particle->setP4(jets[i].px(), jets[i].py(), jets[i].pz(), jets[i].E());
			particle->setName("jet");
			vector<PseudoJet> particlesInJet = cs.constituents(jets[i]);
			for (unsigned j = 0; j < particlesInJet.size(); j++) {
				particle->linkSoft(_particles[particlesInJet[j].user_index()], "particle_in_jet");
			}
			eventView->insertObject(particle);
		}
		event->insertObject(eventView);
	}

	// unique type
	static const std::string &getStaticType() {
		static std::string type("FastJetModule");
		return type;
	}

	// same as dynamic type
	const std::string &getType() const {
		return getStaticType();
	}

	bool isRunnable() const {
		return false;
	}

	bool isExecutable() const {
		// this module does not provide events, so return false
		return false;
	}

	void destroy() throw (std::runtime_error) {
		delete this;
	}
		
};
PXL_MODULE_INIT(FastJetModule)
PXL_PLUGIN_INIT
